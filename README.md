# Projet e-commerce

L'objectif de ce projet sera de réaliser un site de vente en ligne par groupes de 4 personnes en 4 semaines.
A votre équipe de trouver un sujet innovant et motivant (Que vous seriez fier(e) de présenter à vos futurs entretiens).
Le thème du site est "libre" (ça veut dire que si le thème est éthiquement limite, il sera refusé)
Les contraintes sont les suivantes : 
### Conception
* Réalisation de diagrammes de Use Case
* Réalisation d'un diagramme de classe
* Réalisation de maquettes (wireframe) mobile first
### Technique
* Utilisation Symfony 4.3 avec la stack complète (Doctrine, sécurité, twig, etc.)
* Application responsive (bootstrap/bootswatch/etc.)
* Utilisation de git/gitlab pour le travail en groupe
### Organisation Agile
* Faire au moins 4 User stories sur le modèle de celles proposées
* 4 sprints (un par semaine) avec les fonctionnalités à livrer
    * Au début de chaque sprint, définir le livrable attendu, les tâches à réaliser pour puis faire un mail à votre groupe de review indiquant les fonctionnalités priorisées (buy a feature) à livrer/presenter en fin de semaine
    * En début de chaque après midi, faire un daily scrum de 15 minutes max pour échanger sur vos soucis
    * A la fin de chaque sprint, faire une review avec le groupe reviewer (20 minutes max). Garder une trace de la review dans une issue gitlab avec des cases à cocher.
    * Terminer la semaine par une retrospective d'équipe avec un formateur
* Utiliser la partie Issues et les Boards du projet gitlab pour définir vos tâches et votre kanban

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.


## User Stories

1. En tant qu'utilisateur.ice, je veux pouvoir visualiser la liste des produits filtrés pour faciliter mon choix 
2. En tant visiteur.euse je veux pouvoir visualiser la liste des produits pour décider si je veux m'inscrire.
3. En tant qu'utilisateur.ice je veux pouvoir regarder le descriptif d'un film pour le rajouter à mon panier
4. //En tant qu'utilisateur.ice, je veux pouvoir ajouter des produits à mon panier pour passer rapidement ma commande
5. En tant qu'utilisateur.ice, je veux pouvoir laisser un avis sûr un produit
6. En tant qu'utilisateur.ice je veux pourvoir vider mon panier
7. En tant que visiteur.euse, je veux pouvoir créer mon compte/ se connecter pour accéder à des fonctionnalités supplémentaires du site
8. En tant qu'administrateur.ice, je veux pouvoir gérer les produits disponibles afin de modifier le catalogue
    Rappel : vous pouvez découper les user story (culture du backlog)
9. En tant qu'administrateur.ice je veux pouvoir supprimer les avis des utilisateur.ices
10. En tant qu'administrateur.ice je veux pouvoir supprimer les comptes des utilisateur.ices

### Uml 



![ClassDiagram1](uml/diagramClass.png)


## Sprints
### Sprint Semaine 1

### Sprint Semaine 2
- faire les templates et méthodes pour un user non connecté et connecté.

### Sprint Semaine 3
- faire les tests : ***** (peer-programming)
- créer l'admin + routes selon roles : ** (=> Max pendant la demi-heure dispo Lundi avant 15h)
- V1 du panier : ajout & suupression de movies / template : *****
- finir methode search ********
                                                                                                                                              