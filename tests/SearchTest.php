<?php

namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class SearchTest extends WebTestCase
{
  public function setUp() {
    $process = new Process(['php', 'bin/console', 'do:fi:lo']);
    $process->run();
    
}



  public function testCategory()
  {

    $client = static::createClient();
    $crawler = $client->request('GET', '/');
    

    $this->assertResponseIsSuccessful();

    $value = $crawler->filter('#selectCat option:contains("genre0")')->attr('value');
    $form = $crawler->selectButton('Search')->form();

    $form['selectedCategory']->select($value);

    $client->submit($form);

    $crawler = $client->followRedirects();

    $this->assertSelectorExists('.card');
    $this->assertCount(1,['.card']);
  }

      public function testSearch()
      {
  
          $client = static::createClient();
          $crawler = $client->request('GET', '/');
          
          $this->assertResponseIsSuccessful();
          
          $form = $crawler->selectButton('Recherche')->form();
          $form['search'] = 'film1';
          $client->submit($form);
          $crawler = $client->followRedirects();
          
          $this->assertSelectorExists('.card');
          $this->assertCount(1,['.card']);
    }
}
