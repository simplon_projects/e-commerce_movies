<?php

namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class UserTest extends WebTestCase
{

    public function setUp() {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
        
    }


     public function testRegister()
     {
         $client = static::createClient();
         $crawler = $client->request('GET', '/register');
         $this->assertResponseIsSuccessful();
         //On récupère le formulaire en se basant sur le texte du button submit
         $form = $crawler->selectButton('Register')->form();
         $form['user[firstName]'] = 'test';
         $form['user[lastName]'] = 'test';
         $form['user[username]'] = 'fromtest';
         $form['user[mail]'] = 'test@test.com';
         $form['user[password][first]'] = '1234';
         $form['user[password][second]'] = '1234';
         $client->submit($form);
         $this->assertResponseRedirects('/login');
     }

    public function testLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('Login')->form();
        $form['_username'] = "user0";
        $form['_password'] = "1234";
        $client->submit($form);

        $crawler = $client->followRedirect();

        //le var dump permet d'afficher tout le code html de la page dans le terminal
       // var_dump($crawler->filter('body')->text());
        // $this->assertSelectorTextContains('[for="user_password_first"]', 'This value is not valid.');
       //  $this->assertResponseRedirects('/');
        $this->assertCount(1, $crawler->filter('.logout'));
    }
   
}
