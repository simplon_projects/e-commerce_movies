<?php

namespace App\Controller;

use App\Entity\Movies;
use App\Repository\MoviesRepository;
use App\Repository\TypeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MoviesController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(MoviesRepository $moviesRepository)
    {
        return $this->render('home.html.twig', [
            'movies' => $moviesRepository->findAll()
        ]);
    }


    /**
     * @Route("movies/index", name="movies_index")
     */
    public function index(MoviesRepository $moviesRepository)
    {
        return $this->render('movies/index.html.twig', [
            'movies' => $moviesRepository->findAll()
        ]);
    }



    /**
     * @Route("/movies/{id}", name="movies_show", methods={"GET"})
     */
    public function show(Movies $movie, TypeRepository $typeRepository): Response
    {
        return $this->render('movies/show.html.twig', [
            'movie' => $movie,
            'category' => $typeRepository->findAll()

        ]);
    }



    /**
     * @Route("/movies/{id}/addToCart", name="movies_addToCart")
     */
    public function addToCart(ObjectManager $manager, Movies $movie)
    {

        // $cart = new Cart();

        // $cart->setUser($this->getUser());
        // $cart->addCartContent($movie);
        // $manager->persist($cart);
        // $manager->flush();

        return $this->redirectToRoute('user_cart');
    }
}
