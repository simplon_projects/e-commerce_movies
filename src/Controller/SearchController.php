<?php

namespace App\Controller;

use App\Entity\Movies;
use App\Repository\MoviesRepository;
use App\Repository\TypeRepository;
use PHPUnit\Util\Type;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/search")
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/", name="search_movies", methods={"GET"})
     */
    public function search(MoviesRepository $moviesRepository, TypeRepository $typeRepository, Request $request)
    {
        $cat = $request->get('selectedCategory');
        $categorie = $typeRepository->findAll();
        $movies = [];

        if ($cat === null) {
            $movies = $moviesRepository->findAll();
        } else {
            $results = $typeRepository->find($cat);
            if (!$results) {
                $movies = $moviesRepository->findAll();
            } else {
                $movies = $results->getMovies();
            }
        }
        return $this->render('movies/searchMovies.html.twig', [
            'movies' => $movies,
            'category' => $categorie
        ]);
    }

    /**
     * @Route("/results", name="search_results")
     * 
     */
    public function search_results(MoviesRepository $repo, Request $request, TypeRepository $categorie): Response

    {
        
        $categorie = $categorie->findAll();
        $search = $request->get('search');
        $movies = $repo->findByKeyword($search);

    

        return $this->render('movies/searchMovies.html.twig', [
            'category' => $categorie,
            'search' => $search,
            'movies' => $movies
        ]);
    }
}
