<?php

namespace App\Controller;

use App\Entity\Movies;
use App\Entity\User;
use App\Form\MoviesType;
use App\Repository\TypeRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/admin")
     */
class AdminController extends AbstractController
{
    /********************************************************************************/
    /* ROUTES CONCERNANT L'AJOUT, LA MODIF OU LA SUPPRESSION DES MOVIES PAR L'ADMIN:*/
    /********************************************************************************/

    /**
     * @Route("/movies_add", name="movies_add")
     */
    public function addMovie(Request $request,ObjectManager $manager)
    {
        $movie = new Movies();
        $form = $this->createForm(MoviesType:: class, $movie);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($movie);
            $manager->flush(); 
            return $this->redirectToRoute('movies_index');
        }

        return $this->render('admin/movies/add.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/movies_edit/{id}", name="movies_edit", methods={"GET","POST"})
     */
    public function editMovie(Request $request, Movies $movie): Response
    {
        $form = $this->createForm(MoviesType::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('movies_index');
        }

        return $this->render('admin/movies/edit.html.twig', [
            'movie' => $movie,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/movies_delete/{id}", name="movies_delete", methods={"GET","DELETE"})
     */
    public function deleteMovie(Movies $movie, ObjectManager $manager)
    {
        
            $manager->remove($movie);
            $manager->flush();
        

        return $this->redirectToRoute('movies_index');
    }


    /********************************************************************************/
    /* ROUTES CONCERNANT L'AJOUT, LA MODIF OU LA SUPPRESSION DES USERS PAR L'ADMIN:*/
    /********************************************************************************/

    /**
     * @Route("/users", name="users_index")
     */
    public function indexUser(UserRepository $userRepository){
        return $this->render('admin/users/index.html.twig', [
            'users' => $userRepository->findAll()
        ]);
    }
    
    /**
     * @Route("/user_delete/{id}", name="user_delete", methods="GET")
     */
    public function deleteUser(User $user, ObjectManager $manager)
    {
        $manager->remove($user);
        $manager->flush();

        return $this->redirectToRoute('users_index');
    }

    /**
     * @Route("/user_show/{id}", name="user_show")
     */
    public function show(User $user, TypeRepository $repo)
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
            'category' => $repo
        ]);
    }

}
