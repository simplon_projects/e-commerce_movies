<?php

namespace App\Controller;

use App\Entity\Movies;
use App\Entity\User;
use App\Form\ModifyPasswordType;
use App\Form\ModifyUserType;
use App\Repository\TypeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/show/{id}", name="user_show")
     */
    public function show(User $user, TypeRepository $repo)
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
            'category' => $repo
        ]);
    }
    /**
     * @Route("/profil", name="profil_show", methods={"GET"})
     */
    public function showProfile(TypeRepository $typeRepository)
    {
        return $this->render('user/profile/profile.html.twig', [

            'category' => $typeRepository->findAll()

        ]);
    }


    /**
     * @Route("/cart", name="cart")
     */

    public function showCart()
    {
        $user = $this->getUser();

        return $this->render('user/cart/index.html.twig', [
            "user" => $user
        ]);
    }
    /**
     * @Route("/editProfil", name="profil_modify")
     */

    public function profileEdit(Request $request, ObjectManager $manager, TypeRepository $typeRepository)
    {


        $user = $this->getUser();
        $form = $this->createForm(ModifyUserType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('profil_show');
        }

        return $this->render('user/profile/modifyProfile.html.twig', [
            'form' => $form->createView(),
            'category' => $typeRepository->findAll()

        ]);
    }

    /**
     * @Route("/editPassword", name="password_modify")
     */

    public function passwordEdit(Request $request, ObjectManager $manager, TypeRepository $typeRepository, UserPasswordEncoderInterface $encoder)
    {


        $user = $this->getUser();
        $form = $this->createForm(ModifyPasswordType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hashedPassword);

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('profil_show');
        }

        return $this->render('user/profile/modifyProfile.html.twig', [
            'form' => $form->createView(),
            'category' => $typeRepository->findAll()

        ]);
    }

    /**
     * @Route("/user/addCart/{id}", name="add_cart")
     */
    public function addToCart(ObjectManager $manager, Movies $movies) {        
        /*On récupère le user actuellement connecté et ensuite on fait
        appel à la méthode addSubscription de celui ci (cette méthode est
        possible car elle existe dans l'entité App\Entity\User) en 
        lui donnant en argument le user qu'on a récupéré via la route */
         $this->getUser()
         ->getCart()
         ->addContent($movies);
         //On flush pour appliquer la modification à la base de données
         $manager->flush();
         //On redirige vers une autre page
         return $this->redirectToRoute('cart');
     }

     /**
     * @Route("/user/deleteCart/{id}", name="delete_cart")
     */
    public function deleteToCart(ObjectManager $manager, Movies $movies) {        
       
         $this->getUser()
         ->getCart()
         ->removeContent($movies);
         
         $manager->flush();
        
         return $this->redirectToRoute('cart');
     }
    
      /**
      * @Route("/user/cart/validation/", name="validation_cart")
      */
      public function final( ){
        return $this->render('user/cart/validation.cart.html.twig', [
            
        ]);  
                            }
}
