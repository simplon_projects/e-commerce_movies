<?php

namespace App\Controller;

use App\Entity\SupportMovies;
use App\Form\SupportMoviesType;
use App\Repository\SupportMoviesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/support/movies")
 */
class SupportMoviesController extends AbstractController
{
   

    /**
     * @Route("/new", name="support_movies_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $supportMovie = new SupportMovies();
        $form = $this->createForm(SupportMoviesType::class, $supportMovie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($supportMovie);
            $entityManager->flush();

            return $this->redirectToRoute('support_movies_index');
        }

        return $this->render('support_movies/new.html.twig', [
            'support_movie' => $supportMovie,
            'form' => $form->createView(),
        ]);
    }

}
