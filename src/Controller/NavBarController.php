<?php

namespace App\Controller;

use App\Repository\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class NavBarController extends AbstractController
{
     public function search(TypeRepository $repo)
    {
        return $this->render('_navSearch.html.twig', [
            'category' => $repo->findAll()
        ]);
    }
}
   