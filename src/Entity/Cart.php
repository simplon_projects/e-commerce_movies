<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="cart")
     */
    private $userCart;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Movies", inversedBy="carts")
     */
    private $content;

    public function __construct()
    {
        $this->userCart = new ArrayCollection();
        $this->content = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserCart(): Collection
    {
        return $this->userCart;
    }

    public function addUserCart(User $userCart): self
    {
        if (!$this->userCart->contains($userCart)) {
            $this->userCart[] = $userCart;
            $userCart->setCart($this);
        }

        return $this;
    }

    public function removeUserCart(User $userCart): self
    {
        if ($this->userCart->contains($userCart)) {
            $this->userCart->removeElement($userCart);
            // set the owning side to null (unless already changed)
            if ($userCart->getCart() === $this) {
                $userCart->setCart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Movies[]
     */
    public function getContent(): Collection
    {
        return $this->content;
    }

    public function addContent(Movies $content): self
    {
        if (!$this->content->contains($content)) {
            $this->content[] = $content;
        }

        return $this;
    }

    public function removeContent(Movies $content): self
    {
        if ($this->content->contains($content)) {
            $this->content->removeElement($content);
        }

        return $this;
    }
}
