<?php

namespace App\Form;

use App\Entity\Movies;
use App\Entity\Type;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoviesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label'=>'Film'])
            ->add('creator', TextType::class, ['label'=>'Director'])
            ->add('price')
            ->add('duration')
            ->add('description')
            ->add('imagePath', TextType::class, ['label'=>'Poster'])
            ->add('videoPath', TextType::class, ['label'=>'Trailer'])
            ->add('TypeFK', EntityType::class, [
                'label'=>'Kind',
                'class' => Type::class,
                
                'choice_label' => 'categorie',

                'multiple' => false,
                'expanded' => false,
                
            ]);
            //Symfony\Component\Form\FormTypeInterface

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Movies::class,
        ]);
    }
}
