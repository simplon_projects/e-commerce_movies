<?php

namespace App\DataFixtures;
use App\Entity\Cart;

use App\Entity\Movies;
use App\Entity\Type;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\MonologBundle\DependencyInjection\MonologExtension;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        // admin
        $admin = new User();
        $hashedPassword = $this->encoder->encodePassword($admin, '1234');
        $admin->setFirstName("admin")
            ->setLastName("admin")
            ->setMail("admin@live.com")
            ->setPassword($hashedPassword)
            ->setUsername("admin")
            ->setAdmin(true);
        $manager->persist($admin);

            //creation et assignation des valeurs des instances
            
            $user = new User();
            $cart = new Cart();
            
            $sciFi = new Type;
            $sciFi->setCategorie("Sci-Fi");
            $manager->persist($sciFi);

            $comedy = new Type();
            $comedy->setCategorie("Comedy");
            $manager->persist($comedy);

            $horror = new Type();
            $horror->setCategorie("Horror");
            $manager->persist($horror);

            $action = new Type();
            $action->setCategorie("Action");
            $manager->persist($action);

            $romance = new Type();
            $romance->setCategorie("Romance");
            $manager->persist($romance);

            $war = new Type();
            $war->setCategorie("War");
            $manager->persist($war);
            //                                 //


            //Movies
            $batman = new Movies();
            $batman->setCreator("Christopher Nolan")
                ->setDescription("Christopher Nolan steps back into the director's chair for this sequel to Batman Begins, which finds the titular superhero coming face to face with his greatest nemesis -- the dreaded Joker. Christian Bale returns to the role of Batman, Maggie Gyllenhaal takes over the role of Rachel Dawes (played by Katie Holmes in Batman Begins), and Brokeback Mountain star Heath Ledger dons the ghoulishly gleeful Joker makeup previously worn by Jack Nicholson and Cesar Romero. Just as it begins to appear as if Batman, Lt. James Gordon (Gary Oldman), and District Attorney Harvey Dent (Aaron Eckhart) are making headway in their tireless battle against the criminal element, a maniacal, wisecracking fiend plunges the streets of Gotham City into complete chaos. ~ Jason Buchanan, Rovi ")
                ->setDuration(152)
                ->setPrice(15)
                ->setTypeFK($action)
                ->setName("The Dark Knight")
                ->setImagePath("https://resizing.flixster.com/2OwcPMJfCIzlA4UjLiEpRzyQUC0=/206x305/v1.bTsxMTE2NTE2MDtqOzE4MzA5OzEyMDA7ODAwOzEyMDA")
                ->setVideoPath("https://www.youtube.com/embed/TQfATDZY5Y4");
            $manager->persist($batman);

            $theMask = new Movies();
            $theMask->setCreator("Chuck Russell")
            ->setDescription("Hyperactive mayhem results when a mild-manned banker discovers an ancient mask that transforms him into a zany prankster with superhuman powers in this special-effects-intensive comedy. The wildly improvisational Jim Carrey plays Stanley Ipkiss, a decent-hearted but socially awkward guy who one night finds a strange mask. Carrey's trademark energy reveals itself after Stanley puts on the mask and the banker transforms into The Mask, a green-skinned, zoot-suited fireball. The rubber-faced Mask possesses the courage to do the wild, fun things that Stanley fears, including romancing Tina Carlyle (Cameron Diaz). In addition to Carrey's physical talents, the film makes effective use of digital visual effects that bestow the Mask with superhuman speed, insane flexibility, and popping eyes out of a Tex Avery cartoon. The larger narrative, involving the efforts of Tina's gangster boyfriend to destroy Stanley and use the mask's powers for evil, prove less interesting than the anarchic comic set pieces, including a particularly memorable dance number to Cuban Pete. The film delivered enough laughs to become a surprise hit and, along with the same year's Dumb and Dumber, establish Carrey's status as a comedy superstar. ~ Judd Blaise, Rovi ")
            ->setDuration(101)
            ->setName("The Mask")
            ->setPrice(10)
            ->setTypeFK($comedy)
            ->setImagePath("https://resizing.flixster.com/dAtosK178vQit16My2aqw-OXwfE=/206x305/v1.bTsxMTE3Mzk0MztqOzE4MzA5OzEyMDA7ODAwOzEyMDA")
            ->setVideoPath("https://www.youtube.com/embed/LZl69yk5lEY");
            $manager->persist($theMask);


            $fury = new Movies();
            $fury->setName("Fury")
            ->setDescription("April, 1945. As the Allies make their final push in the European Theatre, a battle-hardened army sergeant named Wardaddy (Brad Pitt) commands a Sherman tank and her five-man crew on a deadly mission behind enemy lines. Outnumbered and outgunned, and with a rookie soldier thrust into their platoon, Wardaddy and his men face overwhelming odds in their heroic attempts to strike at the heart of Nazi Germany. (C) Sony")
            ->setDuration(135)
            ->setPrice(13)
            ->setTypeFK($war)
            ->setCreator("David Ayer")
            ->setImagePath("https://resizing.flixster.com/nqpllyM3Cgn-H-Xkq5YKj7Xr3mw=/206x305/v1.bTsxMTE4OTUyNjtqOzE4MzA5OzEyMDA7ODAwOzEyMDA")
            ->setVideoPath("https://www.youtube.com/embed/SKu5lGfRBxc");
            $manager->persist($fury);

            $annabelle = new Movies();
            $annabelle->setName("Annabelle")
            ->setDuration(98)
            ->setPrice(13)
            ->setTypeFK($horror)
            ->setCreator("Gary Dauberman")
            ->setDescription("A couple begins to experience terrifying supernatural occurrences involving a vintage doll shortly after their home is invaded by satanic cultists.")
            ->setImagePath("https://resizing.flixster.com/qTSJNiwAqe5sa4J-9ZT5slXBs-8=/206x305/v1.bTsxMTE4OTcxODtqOzE4MzA5OzEyMDA7ODAwOzEyMDA")
            ->setVideoPath("https://www.youtube.com/embed/paFgQNPGlsg");
            $manager->persist($annabelle);

            $itTwo = new Movies();
            $itTwo->setName("It Chapter Two")
            ->setDuration(149)
            ->setPrice(14)
            ->setTypeFK($horror)
            ->setCreator("Andy Muschietti")
            ->setDescription("Twenty-seven years after their first encounter with the terrifying Pennywise, the Losers Club have grown up and moved away, until a devastating phone call brings them back.")
            ->setImagePath("https://resizing.flixster.com/0BJxdUNCu_XS89l8gaohKy0QWiY=/fit-in/200x296.2962962962963/v1.bTsxMzEzMTgyNTtwOzE4MjQxOzEyMDA7MTA4MDsxMzQ5")
            ->setVideoPath("https://www.youtube.com/embed/xhJ5P7Up3jA");
            $manager->persist($itTwo);


            $stalingrad = new Movies();
            $stalingrad->setName("Stalingrad")
            ->setPrice(10)
            ->setCreator("Joseph Vilsmaier")
            ->setDuration(134)
            ->setDescription(" The story follows a group of German soldiers, from their Italian R&R in the summer of 1942 to the frozen steppes of Soviet Russia and ending with the battle for Stalingrad. ")
            ->setTypeFK($war)
            ->setImagePath("https://m.media-amazon.com/images/M/MV5BZWQzNjA3OTEtYjhkMS00ZWIzLTkxMjItYWE3ZDM1Nzg3ZDdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UY268_CR1,0,182,268_AL_.jpg")
            ->setVideoPath("https://www.youtube.com/embed/fRbOx5OITP0");
            $manager->persist($stalingrad);

            $ironMan = new Movies();
            $ironMan->setName("Iron Man")
            ->setPrice(10)
            ->setDescription("Billionaire industrialist and genius inventor Tony Stark is kidnapped and forced to build a devastating weapon. Instead, using his intelligence and ingenuity, Tony builds a high-tech suit of armor and escapes captivity. When he uncovers a nefarious plot with global implications, he dons his powerful armor and vows to protect the world as Iron Man. ")
            ->setTypeFK($action)
            ->setDuration(126)
            ->setCreator("Jon Favreau")
            ->setImagePath("https://resizing.flixster.com/_STQXxMzf68K_cwQp8eKELrQiqI=/fit-in/200x296.2962962962963/v1.bTsxMTIxODE4OTtqOzE4MzA5OzEyMDA7MTAwMDsxNTAw")
            ->setVideoPath("https://www.youtube.com/embed/8hYlB38asDY");
            $manager->persist($ironMan);    


            $gemdarme = new Movies();
            $gemdarme->setName("Gendarme et les extra-terrestres")
            ->setPrice(14)
            ->setTypeFK($comedy)
            ->setDescription("In this funny French entry in the Gendarme series of films, bungling inspector Cruchot (played by Jerry Lewis-like French comedian De Funes) finds himself trying to save the residents of St. Tropez from oil-guzzling humanoid space aliens. But for their constant thirst for petro-products, the only other way to tell the invaders from people is touch them and see if they sound like empty garbage cans. Soon chaos reigns. ~ Sandra Brennan, Rovi ")
            ->setCreator("Jean Girault")
            ->setDuration(87)
            ->setImagePath("https://resizing.flixster.com/lXtdnbTmcuQe7dpv9NxveU3UW4M=/206x305/v1.bTsxMjE4ODgyNjtqOzE4MjMxOzEyMDA7MjQ5OzM1MA")
            ->setVideoPath("https://www.youtube.com/embed/ROc5JdNZ9ss");
            $manager->persist($gemdarme);


            $interstellar = new Movies();
            $interstellar->setName("Interstellar")
            ->setPrice(14)
            ->setTypeFK($sciFi)
            ->setDuration(169)
            ->setCreator("Christopher Nolan")
            ->setDescription(" With our time on Earth coming to an end, a team of explorers undertakes the most important mission in human history; traveling beyond this galaxy to discover whether mankind has a future among the stars. (C) Paramount
            ")
            ->setImagePath("https://resizing.flixster.com/rqGe4Kvfd99el9bY8-lFkOvQf88=/fit-in/200x296.2962962962963/v1.bTsxMTE5MDg2MDtqOzE4MzA5OzEyMDA7ODAwOzEyMDA")
            ->setVideoPath("https://www.youtube.com/embed/zSWdZVtXT7E");
            $manager->persist($interstellar);


            $inception = new Movies();
            $inception->setName("Inception")
            ->setPrice(13)
            ->setTypeFK($sciFi)
            ->setDuration(148)
            ->setDescription("Visionary filmmaker Christopher Nolan (Memento, The Dark Knight) writes and directs this psychological sci-fi action film about a thief who possesses the power to enter into the dreams of others. Dom Cobb (Leonardo DiCaprio) doesn't steal things, he steals ideas. By projecting himself deep into the subconscious of his targets, he can glean information that even the best computer hackers can't get to. In the world of corporate espionage, Cobb is the ultimate weapon. But even weapons have their weakness, and when Cobb loses everything, he's forced to embark on one final mission in a desperate quest for redemption. This time, Cobb won't be harvesting an idea, but sowing one. Should he and his team of specialists succeed, they will have discovered a new frontier in the art of psychic espionage. They've planned everything to perfection, and they have all the tools to get the job done. Their mission is complicated, however, by the sudden appearance of a malevolent foe that seems to know exactly what they're up to, and precisely how to stop them. ~ Jason Buchanan, Rovi ")
            ->setCreator("Christopher Nolan")
            ->setImagePath("https://resizing.flixster.com/cZVU45vSaFFv0WGcProRzA5YawI=/206x305/v1.bTsxMTE2NjcyNTtqOzE4MzA5OzEyMDA7ODAwOzEyMDA")
            ->setVideoPath("https://www.youtube.com/embed/YoHD9XEInc0");
            $manager->persist($inception);

            $start = new Movies();
            $start->setName("A start is born")
            ->setPrice(13)
            ->setTypeFK($romance)
            ->setDuration(135)
            ->setCreator("Bradley Cooper")
            ->setDescription("In A Star Is Born, Bradley Cooper and Lady Gaga fuse their considerable talents to depict the raw and passionate tale of Jack and Ally, two artistic souls coming together, on stage and in life. Theirs is a complex journey through the beauty and the heartbreak of a relationship struggling to survive. In this new take on the iconic love story, four-time Oscar nominee Cooper (American Sniper, American Hustle, Silver Linings Playbook), makes his directorial debut, and also stars alongside multiple award-winning, Oscar-nominated music superstar Gaga in her first leading role in a major motion picture. Cooper portra")
            ->setImagePath("https://resizing.flixster.com/XWhTWsOf1IMXcARWixPeqzTxVM0=/fit-in/200x296.2962962962963/v1.bTsxMjc2MTM0NTtqOzE4MjM3OzEyMDA7NTA5Ozc1NQ")
            ->setVideoPath("https://www.youtube.com/embed/nSbzyEJ8X9E");
            $manager->persist($start);

            $blueValentine = new Movies();
            $blueValentine->setName("Blue valentine")
            ->setPrice(10)
            ->setDuration(118)
            ->setTypeFK($romance)
            ->setCreator("Andrij Parekh, Derek Cianfrance")
            ->setDescription("A complex portrait of a contemporary American marriage, Blue Valentine tells the story of David and Cindy, a couple who have been together for several years but who are at an impasse in their relationship. While Cindy has blossomed into a woman with opportunities and options, David is still the same person he was when they met, and he is unable to accept either Cindy's growth or his lack of it. Innovatively structured, the narrative unfolds in two distinct time frames, juxtaposing scenes of first love and youthful sexuality with those of disenchantment and discord. ")
            ->setImagePath("https://resizing.flixster.com/4ohHrvIeA1WCyHsDe94OJ2s3vVg=/206x305/v1.bTsxMTIzMDY3MjtqOzE4MzA5OzEyMDA7MTIwMDsxNjAw")
            ->setVideoPath("https://www.youtube.com/embed/aILx69WrRhQ");
            $manager->persist($blueValentine);

            //Cart  
            $cart->addContent($batman)
            ->addUserCart($user);
            $manager->persist($cart);
          

            //User
            $hashedPassword = $this->encoder->encodePassword($user, '1234');
            $user->setFirstName("userTest")
                ->setLastName("Test")
                ->setCart($cart)
                ->setMail("user@live.com")
                ->setPassword($hashedPassword)
                ->setUsername("user")
                ->setAdmin(false);
            $manager->persist($user);
            
    //        for ($y=0; $y<3; $y++)
    //        {
    //            $cart = new Cart();
    //            $cart->setContent([19,22])
    //            ->setPrice(29);
    //            $manager->persist($cart);
    //        }
      $manager->flush();
    }
}
