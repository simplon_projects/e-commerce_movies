<?php

namespace App\Repository;

use App\Entity\Movies;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Movies|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movies|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movies[]    findAll()
 * @method Movies[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoviesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movies::class);
    }

    /**
     * @return Product[]
     */
    public function findByKeyword(string $keyword)
    {

    // on met les "%" avant et après le mot clé pour qu'il cherche ce qui contient mm si il y a des choses avant et apres (doit etre fait à l'exterieur de la requette SQL)
        $key = '%'.$keyword.'%';
        
        $entityManager = $this->getEntityManager();

        // on fait une requette "FROM App\Entity\..." et pas directement sur une Table SQL
        // il faut donc mettre un alias (ici "product"), pour qu'il la prenne en compte 
        // il va la considérer comme une "instance de la table" et chercher dedans.

        // là je le fait chercher dans le nom, la categorie et la description, parce que à priori c est les endroits ou un mot clé peut sortir
        $query = $entityManager->createQuery(
            'SELECT movies
            FROM App\Entity\Movies movies
            WHERE movies.name LIKE :key 
            OR movies.creator LIKE :key
            OR movies.description LIKE :key'
        )->setParameter('key', $key);

        // returns an array of Product objects
        return $query->getResult();
    }


    // /**
    //  * @return Movies[] Returns an array of Movies objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Movies
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
